<?php

namespace Practice\Stores\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class GeoCoordinates extends AbstractHelper
{
    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * LatLongCoordinates constructor.
     * @param Context $context
     * @param Data $dataHelper
     */
    public function __construct(
        Context $context,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
    }

    public function getGeoLL($address)
    {
        if (!empty($address)) {
            $formattedAddr = urlencode($address);
            $key = $this->dataHelper->getApiKey();
            $geocodeFromAddr = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address={$formattedAddr}&key={$this->dataHelper->getApiKey()}");
            $output = json_decode($geocodeFromAddr,true);

            if ($output['status'] == 'OK') {
                $data = [];
                $data['latitude'] = isset($output['results'][0]['geometry']['location']['lat']) ? $output['results'][0]['geometry']['location']['lat'] : "";
                $data['longitude'] = isset($output['results'][0]['geometry']['location']['lng']) ? $output['results'][0]['geometry']['location']['lng'] : "";

                if (!empty($data)) {
                    return $data;
                } else {
                    return false;
                }
            } else {
                $this->_logger->error($output['status']);
                return false;
            }
        }
    }
}
