<?php

namespace Practice\Stores\Block;

use Magento\Framework\View\Element\Template;
use Practice\Stores\Model\StoresFactory as StoresFactory;
use Practice\Stores\Model\Stores;

/**
 * Class Index
 * @package Practice\Stores\Block
 */
class Index extends Template
{
    /**
     * @var StoresFactory
     */
    protected $storesFactory;

    /**
     * @var Stores
     */
    protected $collection;

    /**
     * Index constructor.
     * @param Template\Context $context
     * @param StoresFactory $storesFactory
     * @param Stores $collection
     */
    public function __construct(
        Template\Context $context,
        StoresFactory $storesFactory,
        Stores $collection
    ) {
        $this->storesFactory = $storesFactory;
        $this->collection = $collection;
        parent::__construct($context);
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Stores'));
        if ($this->getStoresCollection()) {
            $pager = $this->getLayout()->createBlock( 'Magento\Theme\Block\Html\Pager', 'stores.stores' )->setAvailableLimit(array(15=>15,20=>20,40=>40))->setShowPerPage(true)->setCollection( $this->getStoresCollection() );
            $this->setChild('pager', $pager);
            $this->getStoresCollection()->load();
        }
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    public function getStoresCollection() {
        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 15;
        $storesCollection = $this->storesFactory->create()->getCollection();
        $storesCollection->setPageSize($pageSize);
        $storesCollection->setCurPage($page)->getData();

        return $storesCollection;
    }
}
