<?php

namespace Practice\Stores\Block\Adminhtml\Page\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Practice\Stores\Api\StoresRepositoryInterface;

class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var StoresRepositoryInterface
     */
    protected $storesRepository;

    /**
     * GenericButton constructor.
     * @param Context $context
     * @param StoresRepositoryInterface $storesRepository
     */
    public function __construct(
        Context $context,
        StoresRepositoryInterface $storesRepository
    ) {
        $this->context = $context;
        $this->storesRepository = $storesRepository;
    }

    /**
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStoreId()
    {
        try {
            return $this->storesRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
