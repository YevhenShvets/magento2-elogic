<?php

namespace Practice\Stores\Block;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Cms\Model\Page as PageModel;
use Magento\Framework\View\Element\Context;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\View\Page\Config;

/**
 * Class Page
 * @package Practice\Stores\Block
 */
class Page extends AbstractBlock implements IdentityInterface
{
    /**
     * @var PageModel
     */
    protected $_page;

    /**
     * Page factory
     *
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var Config
     */
    protected $pageConfig;

    /**
     * Page constructor.
     * @param Context $context
     * @param PageModel $page
     * @param PageFactory $pageFactory
     * @param Config $pageConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        PageModel $page,
        PageFactory $pageFactory,
        Config $pageConfig,
        array $data = []
    ) {
        $this->_page = $page;
        $this->_pageFactory = $pageFactory;
        $this->pageConfig = $pageConfig;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve Page instance
     *
     * @return PageModel
     */
    public function getPage()
    {
        if (!$this->hasData('page')) {
            if ($this->getPageId()) {
                /** @var \Magento\Cms\Model\Page $page */
                $page = $this->_pageFactory->create();
            } else {
                $page = $this->_page;
            }
            $this->setData('page', $page);
        }
        return $this->getData('page');
    }
//
//    protected function _prepareLayout()
//    {
//        $page = $this->getPage();
//        $this->pageConfig->addBodyClass('cms-' . $page->getIdentifier());
//        $metaTitle = $page->getMetaTitle();
//        $this->pageConfig->getTitle()->set($metaTitle ? $metaTitle : $page->getTitle());
//        $this->pageConfig->setKeywords($page->getMetaKeywords());
//        $this->pageConfig->setDescription($page->getMetaDescription());
//
//        return parent::_prepareLayout();
//    }

    public function getIdentities()
    {
        return [\Practice\Stores\Model\Stores::CACHE_TAG . '_' . $this->getPage()->getId()];
    }
}
