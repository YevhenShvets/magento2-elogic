<?php


namespace Practice\Stores\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Practice\Stores\Helper\Image as ImageHelper;

class View extends Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var ImageHelper
     */
    protected $imageHelper;


    /**
     * View constructor.
     * @param Template\Context $context
     * @param Registry $coreRegistry
     * @param ImageHelper $imageHelper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $coreRegistry,
        ImageHelper $imageHelper,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->imageHelper = $imageHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get news information from register which saved in controller
     *
     * @return \Practice\Stores\Model\Stores
     */
    public function getStoreInformation()
    {
        return $this->_coreRegistry->registry('storeData');
    }

    public function getStoreImageUrl($store)
    {
        return $this->imageHelper->getImageUrl($store->getImage());
    }
}
