<?php


namespace Practice\Stores\Service;


use Magento\Framework\Exception\NoSuchEntityException;
use phpDocumentor\Reflection\Types\This;
use Practice\Stores\Api\Data\StoresInterface;
use Practice\Stores\Api\StoresManagementInterface;
use Practice\Stores\Model\StoresFactory;
use Practice\Stores\Model\ResourceModel\Stores as StoresResource;

class StoresManagement
{
    /**
     * @var StoresFactory
     */
    protected $storesFactory;

    /**
     * @var StoresResource
     */
    protected $resource;

    /**
     * StoresManagement constructor.
     * @param StoresFactory $storesFactory
     * @param StoresResource $stores
     */
    public function __construct(StoresFactory $storesFactory, StoresResource $stores)
    {
        $this->storesFactory = $storesFactory;
        $this->resource = $stores;
    }

    /**
     * @return StoresInterface
     */
    public function getEmptyObject(): StoresInterface
    {
        return $this->storesFactory->create();
    }

    /**
     * @param StoresInterface $stores
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(StoresInterface $stores)
    {
        $this->resource->save($stores);
    }

    public function getById($id)
    {
        $store = $this->storesFactory->create();
        $store->load($id);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $id));
        }
        $this->identityMap->add($store);

        return $store;
    }
}
