<?php

namespace Practice\Stores\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Message\ManagerInterface;
use Practice\Stores\Helper\GeoCoordinates;

/**
 * Class StoresSaveBeforeObserver
 * @package Practice\Stores\Observer
 */
class StoresSaveBeforeObserver implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var GeoCoordinates
     */
    protected $geoCoordinates;

    /**
     * StoresSaveBeforeObserver constructor.
     * @param ManagerInterface $messageManager
     * @param GeoCoordinates $geoCoordinates
     */
    public function __construct(
        ManagerInterface $messageManager,
        GeoCoordinates $geoCoordinates
    ) {
        $this->messageManager = $messageManager;
        $this->geoCoordinates = $geoCoordinates;
    }

    public function execute(Observer $observer)
    {
        /** @var \Practice\Stores\Api\Data\StoresInterface $store */
        $store = $observer->getData('practice_stores');

        if ($store->getData("longitude") == null || $store->getData("latitude") == null) {
            $address = $store->getData("address");
            try {
                $coordinates = $this->geoCoordinates->getGeoLL($address);
                if($coordinates) {
                    $store->setData("latitude", $coordinates['latitude']);
                    $store->setData("longitude", $coordinates['longitude']);
                }
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }

        }
    }
}
