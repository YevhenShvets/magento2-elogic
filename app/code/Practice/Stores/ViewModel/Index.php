<?php

namespace Practice\Stores\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Practice\Stores\Model\Stores as StoresFactory;

/**
 * Class Index
 * @package Practice\Stores\ViewModel
 */
class Index implements ArgumentInterface
{
    /**
     * @var StoresFactory
     */
    protected $storesFactory;

    public function __construct(StoresFactory $storesFactory)
    {
        $this->storesFactory = $storesFactory;
    }

    public function getStoresCollection() {
        $stores = $this->storesFactory->create();

        return "fdsds";
    }
}
