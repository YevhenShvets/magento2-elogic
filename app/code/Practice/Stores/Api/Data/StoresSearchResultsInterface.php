<?php
namespace Practice\Stores\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * @api
 */
interface StoresSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get author list.
     *
     * @return \Practice\Stores\Api\Data\StoresInterface[]
     */
    public function getItems();

    /**
     * Set authors list.
     *
     * @param \Practice\Stores\Api\Data\StoresInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
