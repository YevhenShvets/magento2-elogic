<?php


namespace Practice\Stores\Api\Data;

/**
 * Interface StoresInterface
 * @package Practice\Stores\Api\Data
 * @api
 */
interface StoresInterface
{
    const ID = 'id';
}
