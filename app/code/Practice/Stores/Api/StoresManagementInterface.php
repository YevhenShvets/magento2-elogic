<?php
//delete
namespace Practice\Stores\Api;

use Practice\Stores\Api\Data\StoresInterface;

/**
 * Interface StoresManagementInterface
 * @package Practice\Stores\Api
 * @api
 */
interface StoresManagementInterface
{
    /**
     * @return StoresInterface
     */
    public function getEmptyObject();

    /**
     * @param StoresInterface $post
     * @return void
     */
    public function save(StoresInterface $post);

    public function getById($id);
}
