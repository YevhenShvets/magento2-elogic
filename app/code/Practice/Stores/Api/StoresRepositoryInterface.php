<?php
namespace Practice\Stores\Api;


use Magento\Framework\Api\SearchCriteriaInterface;
use Practice\Stores\Api\Data\StoresInterface;

/**
 * @api
 */
interface StoresRepositoryInterface
{
    /**
     * @param StoresInterface $store
     * @return StoresInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(StoresInterface $store);

    /**
     * @param int $storeId
     * @return StoresInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Practice\Stores\Api\Data\StoresSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param StoresInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(StoresInterface $store);

    /**
     * @param int $storeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeId);

    /**
     * @return StoresInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getEmptyObject();
}
