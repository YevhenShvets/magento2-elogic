<?php

namespace Practice\Stores\Model\Import;


use Practice\Stores\Api\StoresRepositoryInterface;
use Practice\Stores\Model\Import\CustomImport\RowValidatorInterface as ValidatorInterface;
use Practice\Stores\Model\Import\UploaderFactory;
use Practice\Stores\Model\StoresFactory;
use Practice\Stores\Api\StoresManagementInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Model\Import as MagentoImport;
use Magento\ImportExport\Model\Import\AbstractEntity;
use Magento\ImportExport\Model\ImportFactory;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Psr\Log\LoggerInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\ImportExport\Model\Import;
use Magento\Framework\Exception\LocalizedException;
use Magento\Eav\Model\Config;

class StoresImport extends  AbstractEntity
{
    const ENTITY_TYPE = 'shop_import';

    const ID = 'id';
    const NAME = 'name';
    const DESC = 'description';
    const IMAGE = 'image';
    const ADDRESS = 'address';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';

    /** @var array */
    protected $_permanentAttributes = [
        self::ID,
        self::NAME,
        self::DESC,
        self::ADDRESS,
    ];

    /** @var LoggerInterface  */
    protected $logger;

    /** @var CollectionFactory */
    protected $attributeSetCollectionFactory;

    /** @var SearchCriteriaBuilder */
    protected $searchCriteria;

    /** @var array */
    protected $attributeSets;

    /** @var array */
    protected $customEntities;

    /** @var Filesystem */
    protected $filesystem;

    /** @var StoresRepositoryInterface */
    protected $entityRepositoryInterface;

    /** @var StoresFactory */
    protected $customEntityFactory;

    /** @var \Practice\Stores\Model\Import\Uploader */
    protected $fileUploader;

    /** @var UploaderFactory */
    protected $uploaderFactory;

    /** @var \Magento\Framework\Filesystem\Directory\WriteInterface */
    protected $mediaDirectory;

    /** @var Config */
    protected $eavConfig;

    /**
     * StoresImport constructor.
     * @param StoresRepositoryInterface $storesManagementInterface
     * @param StringUtils $string
     * @param ScopeConfigInterface $scopeConfig
     * @param ImportFactory $importFactory
     * @param Helper $resourceHelper
     * @param ResourceConnection $resource
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param LoggerInterface $logger
     * @param CollectionFactory $attributeSetCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteria
     * @param Filesystem $filesystem
     * @param \Practice\Stores\Model\Import\UploaderFactory $uploaderFactory
     * @param Config $eavConfig
     * @param array $data
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        StoresRepositoryInterface $storesManagementInterface,
        StoresFactory $storesFactory,
        StringUtils $string,
        ScopeConfigInterface $scopeConfig,
        ImportFactory $importFactory,
        Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        LoggerInterface $logger,
        CollectionFactory $attributeSetCollectionFactory,
        SearchCriteriaBuilder $searchCriteria,
        FileSystem $filesystem,
        UploaderFactory $uploaderFactory,
        Config $eavConfig,
        array $data = []
    ) {
        parent::__construct($string, $scopeConfig, $importFactory, $resourceHelper, $resource, $errorAggregator, $data);
        $this->entityRepositoryInterface = $storesManagementInterface;
        $this->customEntityFactory = $storesFactory;
        $this->_availableBehaviors = [
            MagentoImport::BEHAVIOR_ADD_UPDATE,
        ];
        $this->searchCriteria = $searchCriteria;
        $this->logger = $logger;
        $this->attributeSetCollectionFactory = $attributeSetCollectionFactory;
        $this->filesystem = $filesystem;
        $this->uploaderFactory = $uploaderFactory;
        $this->eavConfig = $eavConfig;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->masterAttributeCode = self::ID;
    }

    /**
     * Validate each row
     *
     * @param array $rowData Row Data
     * @param int   $rowNum  Row Num
     *
     * @return bool
     *
     * @throws LocalizedException
     */
    public function validateRow(array $rowData, $rowNum)
    {
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * Get Entity type code
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return self::ENTITY_TYPE;
    }

    /**
     * Save each Custom Entities
     *
     * @return void
     *
     * @throws \Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function saveCustomEntities()
    {
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                try {
                    if (!$this->validateRow($rowData, $rowNum)) {
                        continue;
                    }
                    if ($this->getErrorAggregator()->hasToBeTerminated()) {
                        $this->getErrorAggregator()->addRowToSkip($rowNum);
                        continue;
                    }

                    $custom = $this->customEntityFactory->create();
                    //$custom->setData('id', $rowData[self::ID]);
                    $custom->setData('name', $rowData[self::NAME]);
                    $custom->setData('address', $rowData['country'] . ", " . $rowData['city'] . ", " . $rowData[self::ADDRESS]);

                    $this->entityRepositoryInterface->save($custom);

                } catch (\Exception $e)
                {
                    $this->logger->error($e->getMessage(), $rowData);
                    throw new \Exception($e);
                }
            }
        }
    }

    /**
     * Import data
     *
     * @return bool
     * @throws \Exception
     */
    protected function _importData()
    {
        $this->saveCustomEntities();
        $this->_validatedRows = null;

        return true;
    }
}
