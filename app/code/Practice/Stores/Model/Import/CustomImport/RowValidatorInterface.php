<?php

namespace Practice\Stores\Model\Import\CustomImport;

use Magento\Framework\Validator\ValidatorInterface;

interface RowValidatorInterface extends ValidatorInterface
{
    const ERROR_INVALID_TITLE= 'InvalidValueTITLE';
    const ERROR_MESSAGE_IS_EMPTY = 'EmptyMessage';
    const ERROR_TITLE_IS_EMPTY = 'EmptyTitle';
    /**
     * Initialize validator
     *
     * @return $this
     */
    public function init($context);
}
