<?php


namespace Practice\Stores\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Stores extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('practice_stores_stores', 'id');
    }
}
