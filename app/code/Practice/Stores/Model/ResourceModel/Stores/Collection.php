<?php

namespace Practice\Stores\Model\ResourceModel\Stores;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Practice\Stores\Model\Stores;
use Practice\Stores\Model\ResourceModel\Stores as ResourceStores;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'practice_stores_stores';
    protected $_eventObject = 'stores_collection';

    protected function _construct()
    {
        $this->_init(Stores::class, ResourceStores::class);
    }
}
