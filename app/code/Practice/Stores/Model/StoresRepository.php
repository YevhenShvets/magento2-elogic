<?php

namespace Practice\Stores\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\NoSuchEntityException;
use Practice\Stores\Api\StoresRepositoryInterface;
use Practice\Stores\Api\Data\StoresSearchResultsInterfaceFactory;
use Practice\Stores\Api\Data\StoresInterfaceFactory;
use Practice\Stores\Api\Data\StoresInterface;
use Practice\Stores\Model\ResourceModel\Stores as ResourceStores;
use Practice\Stores\Model\ResourceModel\Stores\Collection;
use Practice\Stores\Model\ResourceModel\Stores\CollectionFactory as StoresCollectionFactory;

class StoresRepository implements StoresRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];
    /**
     * @var ResourceStores
     */
    protected $resource;
    /**
     * @var StoresCollectionFactory
     */
    protected $storesCollectionFactory;
    /**
     * @var StoresSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var StoresInterfaceFactory
     */
    protected $storesInterfaceFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    public function __construct(
        ResourceStores $resource,
        StoresCollectionFactory $storesCollectionFactory,
        StoresSearchResultsInterfaceFactory $storesSearchResultsInterfaceFactory,
        StoresInterfaceFactory $storesInterfaceFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->resource                 = $resource;
        $this->storesCollectionFactory  = $storesCollectionFactory;
        $this->searchResultsFactory     = $storesSearchResultsInterfaceFactory;
        $this->storesInterfaceFactory   = $storesInterfaceFactory;
        $this->dataObjectHelper         = $dataObjectHelper;
    }

    public function getEmptyObject(): StoresInterface
    {
        return $this->storesInterfaceFactory->create();
    }


    public function save(StoresInterface $store)
    {
        /** @var StoresInterface|\Magento\Framework\Model\AbstractModel $store */
        try {
            $this->resource->save($store);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the store: %1',
                $exception->getMessage()
            ));
        }
        return $store;
    }

    /**
     * Retrieve Author.
     *
     * @param int $storeId
     * @return \Practice\Stores\Api\Data\StoresInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId)
    {
        if (!isset($this->instances[$storeId])) {
            /** @var \Practice\Stores\Api\Data\StoresInterface|\Magento\Framework\Model\AbstractModel $store */
            $store = $this->storesInterfaceFactory->create();
            $this->resource->load($store, $storeId);
            if (!$store->getData('id')) {
                throw new NoSuchEntityException(__('Requested author doesn\'t exist'));
            }
            $this->instances[$storeId] = $store;
        }
        return $this->instances[$storeId];
    }

    /**
     * Retrieve pages matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Practice\Stores\Api\Data\StoresSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Practice\Stores\Api\Data\StoresSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Practice\Stores\Model\ResourceModel\Stores\Collection $collection */
        $collection = $this->storesCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
//        foreach ($searchCriteria->getFilterGroups() as $group) {
//            $this->addFilterGroupToCollection($group, $collection);
//        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Practice\Stores\Api\Data\StoresInterface[] $stores */
        $stores = [];
        /** @var \Practice\Stores\Model\Stores $store */
        foreach ($collection as $store) {
            /** @var \Practice\Stores\Api\Data\StoresInterface $storesDataObject */
            $storesDataObject = $this->storesInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($storesDataObject, $store->getData(), StoresInterface::class);
            $stores[] = $storesDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($stores);
    }

    public function delete(StoresInterface $store)
    {
        /** @var \Practice\Stores\Api\Data\StoresInterface|\Magento\Framework\Model\AbstractModel $store */
        $id = $store->getData('id');
        try {
            unset($this->instances[$id]);
            $this->resource->delete($store);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove store %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    public function deleteById($storeId)
    {
        $store = $this->getById($storeId);
        return $this->delete($store);
    }
}
