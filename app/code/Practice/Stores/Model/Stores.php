<?php

namespace Practice\Stores\Model;

use Magento\Framework\Model\AbstractModel;
use Practice\Stores\Api\Data\StoresInterface;
use Practice\Stores\Model\ResourceModel\Stores as ResourceStores;

/**
 * Class Stores
 * @package Practice\Stores\Model
 */
class Stores extends AbstractModel implements StoresInterface
{
    const CACHE_TAG = 'practice_stores_stores';

    protected $_cacheTag = 'practice_stores_stores';
    protected $_eventObject = 'practice_stores';

    protected $_eventPrefix = 'practice_stores_stores';

    protected function _construct()
    {
        $this->_init(ResourceStores::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
