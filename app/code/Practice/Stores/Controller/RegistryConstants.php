<?php
namespace Practice\Stores\Controller;

class RegistryConstants
{
    /**
     * Registry key where current stores ID is stored
     */
    const CURRENT_STORES_ID = 'current_stores_id';
}
