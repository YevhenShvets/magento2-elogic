<?php

namespace Practice\Stores\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory as PageFactory;
use Practice\Stores\Model\Stores;
use Practice\Stores\Model\StoresFactory as StoresFactory;

/**
 * Class Index
 * @package Practice\Stores\Controller\Index
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var StoresFactory
     */
    protected $storesFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        StoresFactory $storesFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->storesFactory = $storesFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->set("Stores");

        /** @var \Magento\Theme\Block\Html\Breadcrumbs */
        $breadcrumbs = $page->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->_url->getUrl('')
            ]
        );
        $breadcrumbs->addCrumb('allstores',
            [
                'label' => __('All Stores'),
                'title' => __('All Stores')
            ]
        );
        return $page;
    }

}
