<?php


namespace Practice\Stores\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory as PageFactory;
use Practice\Stores\Model\Stores;
use Practice\Stores\Model\StoresFactory as StoresFactory;

class View extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var StoresFactory
     */
    protected $storesFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        StoresFactory $storesFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->storesFactory = $storesFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $storeId = $this->getRequest()->getParam('id');
        $store = $this->storesFactory->create()->load($storeId);

        $this->_objectManager->get('Magento\Framework\Registry')
            ->register('storeData', $store);

        $pageFactory = $this->pageFactory->create();

        $pageFactory->getConfig()->getTitle()->set($store->getName());

        $breadcrumbs = $pageFactory->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->_url->getUrl('')
            ]
        );
        $breadcrumbs->addCrumb('allstores',
            [
                'label' => __('All Stores'),
                'title' => __('All Stores'),
                'link' => $this->_url->getUrl('shop')
            ]
        );
        $breadcrumbs->addCrumb('store',
            [
                'label' => $store->getName(),
                'title' => $store->getName()
            ]
        );

        return $pageFactory;
    }

}
