<?php

namespace Practice\Stores\Controller\Adminhtml\Page;

use Magento\Backend\App\Action;
use Practice\Stores\Model\Stores as Stores;

class Delete extends Action
{
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if (!($stores = $this->_objectManager->create(Stores::class)->load($id))) {
            $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        try{
            $stores->delete();
            $this->messageManager->addSuccess(__('Your store has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addError(__('Error while trying to delete store: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}
