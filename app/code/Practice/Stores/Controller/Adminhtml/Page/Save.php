<?php

namespace Practice\Stores\Controller\Adminhtml\Page;


use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Framework\View\Result\PageFactory;
use Practice\Stores\Api\StoresRepositoryInterface;
use Practice\Stores\Api\Data\StoresInterface;
use Practice\Stores\Api\Data\StoresInterfaceFactory;
use Practice\Stores\Api\StoresManagementInterface;

class Save extends \Practice\Stores\Controller\Adminhtml\Stores
{
    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;


    /**
     * @var StoresInterfaceFactory
     */
    protected $storesFactory;


    public function __construct(
        Registry $registry,
        StoresRepositoryInterface $storesRepository,
        PageFactory $resultPageFactory,
        Date $dateFilter,
        Context $context,
        StoresInterfaceFactory $storesFactory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper
    )
    {
        $this->storesFactory = $storesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($registry, $storesRepository, $resultPageFactory, $dateFilter, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        /** @var \Practice\Stores\Api\Data\StoresInterface $store */
        $store = null;
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['id']) ? $data['id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $store = $this->storesRepository->getById((int)$id);
            } else {
                unset($data['id']);
                $store = $this->storesRepository->getEmptyObject();
            }
            if (isset($data['image']) && is_array($data['image'])) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
                $currentStore = $storeManager->getStore();
                $media_url = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $data['image'] = $data['image'][0]['name'];
            } else {
                $data['image'] = null;
            }
            $store->setData($data);
            $this->dataObjectHelper->populateWithArray($store, $data, StoresInterface::class);
            $this->storesRepository->save($store);
            $this->messageManager->addSuccessMessage(__('You saved the store'));
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('*/*/edit', ['id' => $store->getId()]);
            } else {
                $resultRedirect->setPath('*/*');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
//            if ($store != null) {
//                $this->storeAuthorDataToSession(
//                    $this->dataObjectProcessor->buildOutputDataArray(
//                        $store,
//                        StoresInterface::class
//                    )
//                );
//            }
//            $resultRedirect->setPath('shop/page/edit', ['id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the store'));
//            if ($store != null) {
//                $this->storeAuthorDataToSession(
//                    $this->dataObjectProcessor->buildOutputDataArray(
//                        $store,
//                        StoresInterface::class
//                    )
//                );
//            }
//            $resultRedirect->setPath('shop/page/edit', ['id' => $id]);
        }
        return $resultRedirect;
    }
}
