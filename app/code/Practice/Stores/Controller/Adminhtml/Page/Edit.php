<?php

namespace Practice\Stores\Controller\Adminhtml\Page;

use Practice\Stores\Controller\Adminhtml\Stores as StoresController;
use Practice\Stores\Controller\RegistryConstants;

class Edit extends StoresController
{
    /**
     *
     * @return int
     */
    protected function _initStores()
    {
        $storeId = $this->getRequest()->getParam('id');
        $this->coreRegistry->register(RegistryConstants::CURRENT_STORES_ID, $storeId);

        return $storeId;
    }

    /**
     * Edit or create store
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $storeId = $this->_initStores();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Practice_Stores::stores_id');
        $resultPage->getConfig()->getTitle()->prepend(__('Stores'));
//        $resultPage->addBreadcrumb(__('News'), __('News'));
//        $resultPage->addBreadcrumb(__('Authors'), __('Authors'), $this->getUrl('sample_news/author'));

        if ($storeId === null) {
            $resultPage->addBreadcrumb(__('New Store'), __('New Store'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Store'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Store'), __('Edit Store'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->storesRepository->getById($storeId)->getName()
            );
        }
        return $resultPage;
    }
}
