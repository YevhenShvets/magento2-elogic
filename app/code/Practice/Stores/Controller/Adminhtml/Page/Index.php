<?php

namespace Practice\Stores\Controller\Adminhtml\Page;

use \Practice\Stores\Controller\Adminhtml\Stores as StoresController;

class Index extends StoresController
{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Practice_Stores::stores_id');
        $resultPage->getConfig()->getTitle()->prepend((__('Stores')));
        return $resultPage;
    }
}
