<?php

declare(strict_types=1);

namespace Test\Blog\ViewModel;

use Magento\Cms\Api\Data\PageInterface;
use Magento\Cms\Api\Data\PageSearchResultsInterface;
use Magento\Cms\Model\Page;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Test\Blog\Service\PostRepository;
use Test\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;

/**
 * Class Blog
 */
class Blog implements ArgumentInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var PostCollectionFactory
     */
    private $postCollectionFactory;

    /**
     * Blog constructor.
     * @param SerializerInterface $serializer
     * @param PostRepository $postRepository
     * @param UrlInterface $url
     * @param PostCollectionFactory $postCollectionFactory
     */
    public function __construct(
        SerializerInterface $serializer,
        PostRepository $postRepository,
        UrlInterface $url,
        PostCollectionFactory $postCollectionFactory
    ) {
        $this->serializer = $serializer;
        $this->postRepository = $postRepository;
        $this->url = $url;
        $this->postCollectionFactory = $postCollectionFactory;
    }

    /**
     * @return string
     */
    public function getPostsJson(): string
    {
        $postsSearchResult = $this->postRepository->get();

        return $this->serializer->serialize($this->getPosts($postsSearchResult));
    }

    private function getPosts(PageSearchResultsInterface $postsSearchResult)
    {
        $result = [];

        /** @var PageInterface|Page $post */
        foreach ($postsSearchResult->getItems() as $post) {
            $result[] = [
                "id" => $post->getId(),
                "url" => $this->url->getUrl($post->getIdentifier()),
                "title" => $post->getTitle(),
                "published_date" => $post->getData('publish_date'),
                "content" => $this->truncate(strip_tags($post->getContent()), 50),
                "author" => $post->getData('author')
            ];
        }
        return $result;
    }

    private function truncate($phrase, $max_words)
    {
        $phrase_array = explode(' ', $phrase);

        if(count($phrase_array) > $max_words && $max_words > 0) {
            $phrase = implode(' ', array_slice($phrase_array, 0, $max_words)) . '...';
        }
        return $phrase;
    }
}
