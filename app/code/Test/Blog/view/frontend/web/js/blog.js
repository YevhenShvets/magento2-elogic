define(['uiComponent'], function(Component){
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Test_Blog/blog'
        },
        initialize: function () {
            this._super();

            return this;
        },

        getDate: function(value) {
            const date = new Date(value);

            const formatter = new Intl.DateTimeFormat('uk', {month: 'long'});
            const month = formatter.format(date);
            return  month + ' ' + date.getDate() + ', ' + date.getFullYear();
        }
    });
});
