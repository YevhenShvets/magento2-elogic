<?php

namespace Test\Blog\Api;

use Test\Blog\Api\Data\PostInterface;

/**
 * Interface PostRepositoryInterface
 * @package Test\Blog\Api
 * @api
 */
interface PostRepositoryInterface
{
    /**
     * @return PostInterface
     */
    public function get();

    /**
     * @param int $pageId
     * @return PostInterface
     */
    public function getByPageId($pageId): PostInterface;

}
