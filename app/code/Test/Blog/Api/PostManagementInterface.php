<?php

namespace Test\Blog\Api;

use Test\Blog\Api\Data\PostInterface;

/**
 * Interface PostManagementInterface
 * @package Test\Blog\Api
 * @api
 */
interface PostManagementInterface
{
    /**
     * @return PostInterface
     */
    public function getEmptyObject();

    /**
     * @param PostInterface $post
     * @return void
     */
    public function save(PostInterface $post);

}
