<?php
declare(strict_types=1);

namespace Test\Blog\Observer;

use Magento\Cms\Model\Page;
use Magento\Cms\Model\ResourceModel\Page\Collection;
use Test\Blog\Model\ResourceModel\Post\Collection as PostCollection;
use Test\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class PageCollectionLoadAfter
 * @package Test\Blog\Observer
 */
class PageCollectionLoadAfter implements ObserverInterface
{
    /**
     * @var PostCollectionFactory
     */
    private $postCollectionFactory;

    /**
     * PageCollectionLoadAfter constructor.
     * @param PostCollectionFactory $postCollectionFactory
     */
    public function __construct(PostCollectionFactory $postCollectionFactory)
    {
        $this->postCollectionFactory = $postCollectionFactory;
    }

    public function execute(Observer $observer)
    {
        /** @var Collection $entity */
        $collection = $observer->getEvent()->getPageCollection();
        $pageIds = [];

        /** @var Page $item */
        foreach ($collection->getItems() as $item) {
            $pageIds[] = $item->getId();
        }
        $postCollection = $this->postCollectionFactory->create();
        $postCollection->addFieldToFilter('page_id', ['in'=>$pageIds]);

        foreach($postCollection->getItems() as $post) {
            $page = $collection->getItemById($post->getPageId());
            if($page->getId()) {
                $page->setData('author', $post->getData('author'));
                $page->setData('is_post', $post->getData('is_post'));
                $page->setData('publish_date', $post->getData('publish_date'));
            }
        }

    }
}
