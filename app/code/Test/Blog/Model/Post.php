<?php

namespace Test\Blog\Model;


use Magento\Framework\Model\AbstractModel;
use Test\Blog\Api\Data\PostInterface;
use Test\Blog\Model\ResourceModel\Post as PostResource;

class Post extends AbstractModel implements PostInterface
{
    protected function _construct()
    {
        $this->_init(PostResource::class);
    }
}
