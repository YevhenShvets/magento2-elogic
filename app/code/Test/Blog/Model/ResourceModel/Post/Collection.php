<?php

namespace Test\Blog\Model\ResourceModel\Post;

use Test\Blog\Model\Post;
use Test\Blog\Model\ResourceModel\Post as PostResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Test\Blog\Model\ResourceModel\Post
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Post::class, PostResource::class);
    }
}
