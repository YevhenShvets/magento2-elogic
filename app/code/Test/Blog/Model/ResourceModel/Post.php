<?php


namespace Test\Blog\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Post
 * @package Test\Blog\Model\ResourceModel
 */
class Post extends AbstractDb
{

    protected function _construct()
    {
        $this->_init('test_blog_page', 'post_id');
    }
}
