<?php

namespace Test\Blog\Service;

use Test\Blog\Api\Data\PostInterface;
use Test\Blog\Api\PostManagementInterface;
use Test\Blog\Model\PostFactory;
use Test\Blog\Model\ResourceModel\Post as PostResource;

/**
 * Class PostManagement
 * @package Test\Blog\Service
 */
class PostManagement implements PostManagementInterface
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var PostResource
     */
    private $resource;

    /**
     * PostManagement constructor.
     * @param PostFactory $postFactory
     * @param PostResource $resource
     */
    public function __construct(
        PostFactory $postFactory,
        PostResource $resource
    ) {
        $this->postFactory = $postFactory;
        $this->resource = $resource;
    }

    /**
     * @return PostInterface
     */
    public function getEmptyObject(): PostInterface
    {
        return $this->postFactory->create();
    }

    /**
     * @param PostInterface $post
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(PostInterface $post)
    {
        $this->resource->save($post);
    }
}
