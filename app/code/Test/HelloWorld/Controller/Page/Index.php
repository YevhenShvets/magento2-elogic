<?php

namespace Test\HelloWorld\Controller\Page;

use Magento\Framework\App\ResponseInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        echo "Hello World";
    }
}
